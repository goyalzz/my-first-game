package com.goyalzz.gameobjects;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Vicky on 05-01-2016.
 */
public class Bird {

    private Vector2 position;
    private Vector2 velocity;
    private Vector2 acceleration;

    private float rotation; // For handling bird rotation
    private int width;
    private int height;

    public Bird(float x, float y, int width, int height) {
        this.width = width;
        this.height = height;
        position = new Vector2(x, y);
        velocity = new Vector2(0, 0);
        acceleration = new Vector2(0, 460);
    }

//    Vector2 is a powerful built-in libGDX class. If you are not familiar with mathematical vectors,
//      that's okay! We are just going to treat it as an object that can hold two values: an x component and a y component.
//    position.x then refers to the x coordinate, and velocity.y would correspond to the speed in the y direction.
//      acceleration just means change in velocity, just like velocity means change in position.


    public void update(float delta) {

        velocity.add(acceleration.cpy().scl(delta));

        if (velocity.y > 200) {
            velocity.y = 200;
        }

        position.add(velocity.cpy().scl(delta));

    }

    public void onClick() {
        velocity.y = -140;
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getRotation() {
        return rotation;
    }
}
